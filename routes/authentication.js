var router = require('express').Router();
var authController = require('../controllers/authentication')

/* GET users listing. */
router.post('/register', authController.register);
router.post('/login', authController.login);
router.post('/reset/request', authController.requestPasswordReset);
router.post('/reset/do', authController.resetPassword);

module.exports = router;