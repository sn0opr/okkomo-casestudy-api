var router = require('express').Router();
var dashboardController = require('../controllers/dashboard')

/* GET users listing. */
router.get('/overview', dashboardController.overview);

module.exports = router;