var mongoose = require( 'mongoose' );
var crypto = require('crypto');
var jwt = require('jsonwebtoken');

const userSchema = new mongoose.Schema({
    username: {
        type: String,
        unique: true,
        required: true
    },
    email: {
        type: String,
        unique: true,
        required: true
    },
    fullName: {
        type: String,
        required: true
    },
    address: {
        type: String
    },
    hash: {
        type: String,
        required: true
    },
    salt: {
        type: String,
        required: true
    },
    resetPasswordToken: String,
    resetPasswordExpires: Number
})

userSchema.methods.setPassword = function(password) {
    this.salt = crypto.randomBytes(16).toString('hex');
    this.hash = crypto.pbkdf2Sync(password, this.salt, 1000, 64).toString('hex');
};

userSchema.methods.validatePassword = function(password) {
    let hash = crypto.pbkdf2Sync(password, this.salt, 1000, 64).toString('hex');
    return this.hash === hash;
}

userSchema.methods.generateJWT = function() {
    let expireDate = new Date();
    expireDate.setDate(expireDate.getDate() + 7);
    return jwt.sign({
        _id: this.id,
        email: this.email,
        username: this.username,
        fullName: this.fullName,
        exp: parseInt(expireDate.getTime() / 1000),
    }, 'SECRETE*****TOCHANGE')
}

userSchema.methods.generateResetPasswordToken = function() {
    this.resetPasswordToken = crypto.randomBytes(20).toString('hex');
    this.resetPasswordExpires = Date.now() + 3600000
    return this.resetPasswordToken;
}

userSchema.methods.clearResetPasswordToken = function() {
    this.resetPasswordToken = null;
    this.resetPasswordExpires = null;
}

mongoose.model('User', userSchema);