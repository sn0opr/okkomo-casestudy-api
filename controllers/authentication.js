const passport = require('passport');
const mongoose = require('mongoose');
const User = mongoose.model('User');
const transporter = require('../config/mailer');

module.exports.register = (req, res) => {
    console.info('Registring new user');
    let user = new User();
    user.username = req.body.username;
    user.fullName = req.body.fullName;
    user.email  = req.body.email;
    user.password = req.body.password;
    user.address = req.body.address;
    user.setPassword(req.body.password);
    user.save().then(newUser => {
        res.status(200);
        res.json({
            "message": "User created successfully"
        });
    })
    .catch(error => {
        res.status(400);
        res.json({
            error: error
        });
    })
}

module.exports.login = (req, res) => {
    passport.authenticate('local', (err, user, info) => {
        let token;
        if (err) {
            res.status(404).json(err);
            return;
        }

        if (user) {
            token = user.generateJWT();
            res.status(200)
            .json({
                token: token
            });
            return;
        }
        res.status(401)
        .json(info);
    })(req, res);
}

module.exports.requestPasswordReset = (req, res) => {
    User.findOne({
        email: req.body.email
    }).then((user) => {
        if (!user) {
            res.status(400);
            res.json({
                message: 'User not found.'
            })
            return;
        }
        //Generate password reset token
        const resetPasswordToken = user.generateResetPasswordToken();
        user.save().then(() => {
            let mailOptions = {
                from: 'reset@okomo.com',
                to: user.email,
                subject: 'Okomo user password reset',
                text: `<h3>Password Reset Request</h3>
    
                To reset your password, copy the following reset password token in the "reset pssword page"<br>
                <b>Reset Password: </b> ${resetPasswordToken}`
            };
            //Sending email to the user
            transporter.sendMail(mailOptions, err => {
                console.log('Sent reset password email');
                if (err)
                    console.error('Sending reset password email error: ', err);
            });
    
            res.status(200);
            res.json({
                message: 'Token sent by mail'
            })
        });
    })
    .catch(function(err) {
        console.log('Catched');
    })
}

module.exports.resetPassword = (req, res) => {
    let resetToken = req.body.token;
    let newPassword = req.body.newPassword;
    User.findOne({
        resetPasswordToken: resetToken
    }).then(user => {
        console.log('User!!!!!', user);
        if (!user) {
            res.status(400);
            res.json({
                message: 'User not found or invalid token.'
            });
            return;
        } else if (user.resetTokenExpire < Date.now()) {
            res.status(403);
            res.json({
                message: 'Reset password token has been expired'
            });
            return;
        }
        user.setPassword(newPassword);
        user.clearResetPasswordToken();
        user.save().then(() => {
            res.status(200);
            res.json({
                message: 'Password has been changed successfully'
            }) 
        });
    })
}