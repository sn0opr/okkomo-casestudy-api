var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;
var mongoose = require('mongoose');
var User = mongoose.model('User');

passport.use(new LocalStrategy({
    usernameField: 'username'
}, 
(username, password, done) => {
    let searchField = username.indexOf('@') > -1 ? 'email' : 'username';
    User.findOne({
        [searchField]: username
    }, (err, user) => {
        if (err) {
            return done(err)
        }

        if (!user) {
            return done(null, false, {
                message: 'User not found'
            });
        }

        if (!user.validatePassword(password)) {
            return done(null, false, {
                message: 'Password is wrong'
            });
        }
        return done(null, user);
    })
}
))