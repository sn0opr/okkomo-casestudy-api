const nodemailer = require('nodemailer');
const ENV = require('../env');
const transporter = nodemailer.createTransport({
    host: ENV.mailer.host,
    port: ENV.mailer.port,
    auth: {
        user: ENV.mailer.user,
        pass: ENV.mailer.pass
    }
});

module.exports = transporter;